;;(set-frame-font "Mono 13")
(set-frame-font "Fira Code Light 14")
(add-to-list 'load-path "~/mosaic/")
(load "snd-config.el")
(load "nanotag.el")

;;(add-to-list 'load-path "~/Desktop/cell/")
(add-to-list 'load-path "~/Desktop/mosaic/")
(byte-compile-file "~/mosaic/cell.el" t)
;;(require 'cell)
(add-to-list 'auto-mode-alist (cons "\\.cell" 'cell-mode))

(load "mosaic.el")
(mosaic-insinuate-cell)

(set-language-environment "UTF-8")
(set-default-coding-systems 'utf-8)

;; (add-to-list 'default-frame-alist '(inhibit-double-buffering . t))
;; (modify-frame-parameters nil '((inhibit-double-buffering . t)))

(setf safe-local-variable-values
      '((Package . C)
	(Package . CLOS)
	(Syntax . Common-Lisp)))

(add-hook 'before-save-hook 'time-stamp)

(add-to-list 'load-path "~/emacs-config")


(setq image-dired-external-viewer "eom")

;; enable melpa package support
(require 'package) (setq package-enable-at-startup nil) (add-to-list
'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

;; (defvar outline-minor-mode-prefix "\M-#")

;; (require 'outshine)
;; (add-hook 'outline-minor-mode-hook 'outshine-hook-function)
;; (add-hook 'emacs-lisp-mode-hook 'outline-minor-mode)
;; (add-hook 'lisp-mode-hook 'outline-minor-mode)

;; install use-package to manage packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package paren ; highlight matching parentheses
  :init (add-hook 'prog-mode-hook 'show-paren-mode)
  :config (setq show-paren-delay 0))

;; (use-package indent-guide ; show indentation levels
;;   :ensure t
;;   :config (add-hook 'lisp-mode-hook 'indent-guide-mode)
;;   :diminish indent-guide-mode)

(use-package windmove ; focus windows with shift + arrow keys
  :bind (("S-<left>" . windmove-left)
         ("S-<right>" . windmove-right)
         ("S-<up>" . windmove-up)
         ("S-<down>" . windmove-down)))

;; (use-package auto-complete ; auto-completion
;;   :ensure t
;;   :config (progn (ac-config-default)
;;                  (ac-flyspell-workaround)
;;                  (ac-linum-workaround)
;;                  (define-key ac-completing-map "\r" 'ac-complete)
;;                  (setq ac-use-quick-help nil
;;                        ac-ignore-case t
;;                        ac-use-fuzzy t
;;                        ac-trigger-key "TAB"
;;                        ac-auto-start nil)
;;                  (set-face-background 'ac-candidate-face "blue")
;;                  (set-face-foreground 'ac-candidate-face "cyan")
;;                  (set-face-background 'ac-selection-face "magenta")
;;                  (set-face-foreground 'ac-selection-face "yellow"))
;;   :diminish auto-complete-mode)

(use-package powerline ; nicer modeline
  :ensure t
  :config ;; (progn (set-face-attribute 'mode-line nil
          ;;                            :foreground "yellow"
          ;;                            :background "dark olive green"
          ;;                            :underline nil
          ;;                            :box nil)
          ;;        (set-face-attribute 'powerline-inactive1 nil
          ;;                            :foreground "#fff"
          ;;                            :background "gray50"
          ;;                            :inherit 'mode-line)
          ;;        (set-face-attribute 'powerline-inactive2 nil
          ;;                            :foreground "#fff"
          ;;                            :background "gray40"
          ;;                            :inherit 'mode-line)
          ;;        (set-face-attribute 'powerline-active1 nil
          ;;                            :foreground "#fff"
          ;;                            :background "dark violet"
          ;;                            :inherit 'mode-line)
          ;;        (set-face-attribute 'powerline-active2 nil
          ;;                            :foreground "#fff"
          ;;                            :background "slate blue"
          ;;                            :inherit 'mode-line)
                 (setq powerline-display-buffer-size nil
                       x-underline-at-descent-line t)
                 (powerline-default-theme))

(use-package smart-mode-line ; set the modeline
  :ensure t
  :config (progn (setq sml/theme 'respectful
                       sml/shorten-directory t sml/shorten-mode t
                       sml/name-width 40 sml/mode-width 'full
                       sml/line-number-format "%4l"
                       sml/no-confirm-load-theme t)
                 (sml/setup)))

(defun sml/get-directory () "")

;; (use-package rainbow-mode ; highlight color names, hex, rgb, etc with the respective color
;;   :ensure t
 ;;   :config (add-hook 'lisp-mode-hook (lambda () (rainbow-mode 1))))

(setq inhibit-splash-screen t)
(global-set-key (kbd "C-z") 'undo)

(add-to-list 'load-path "~/elisp/")

;; Xelf

(add-to-list 'load-path "~/xelf")
(require 'xelf)
(add-hook 'lisp-mode-hook #'xelf-do-font-lock)
;; (add-to-list 'load-path "~/elo")
;; (require 'elo)
;; (elo-insinuate-emacs)

;(require 'glass)
(setq slime-enable-evaluate-in-emacs t)
;; (setq glass-use-themes nil)
;; (setq glass-use-special-frame nil)
;; (setq glass-font "Mono 8")
(xelf-insinuate-lisp)

;; 

(require 'cl)

;(icomplete-mode)

(defun xelf-grep (string)
  (interactive "sSearch for string in XELF source: ")
  (grep (concat "grep -in \"" string "\" ~/xelf/*.lisp")))

;; (add-to-list 'load-path "/home/dto/elisp/magit-1.2.0")
(require 'magit)
(global-set-key [(control c) (g)] 'magit-status)

;(global-set-key [(control c) (p)] 'paredit-mode)

(setq lpr-command "xpp")
(setq history-length 250)


(add-to-list 'load-path "/home/dto/elisp/org-mode/lisp")
(require 'org-install)

;;(setf org-fontify-emphasized-text nil)

(defvar user-elisp-dir "~/elisp")
(add-to-list 'load-path user-elisp-dir)

(add-to-list 'load-path "~/emacs-config")

;; (require 'color-theme)
;; (setq color-theme-is-global nil)
;; (setq color-theme-is-cumulative nil)
(setq pop-up-windows nil)                  
(setq mouse-autoselect-window nil)

(global-set-key [mouse-3] 'mouse-popup-menubar-stuff)
(setq lisp-indent-function 'common-lisp-indent-function)

(setf mouse-avoidance-mode 'banish)

(defun tweak-xiomacs ()
  (interactive)
  (eval-buffer)
  (color-theme-xiomacs))
 
(global-set-key [(f6)] 'tweak-xiomacs)

;;;; cursor config

(setq-default cursor-type 'box)
(setq messages-buffer-max-lines 400)
(setq blink-cursor-delay 0.1)
(setq blink-cursor-interval 0.2)


      
;;;; moving around the windows and frames

(global-set-key [(control tab)] (lambda ()  
				  (interactive)
				  (other-window 1)))

(global-set-key [(control shift iso-lefttab)] (lambda ()  
				       (interactive)
				       (other-window -1)))

	
;; ;;;; ps-print

 (setf ps-landscape-mode nil)
 (setf ps-line-number 'zebra)
 (setf ps-zebra-stripes t)


(tooltip-mode -1)

(tool-bar-mode 0)
(setq scroll-conservatively 1)
(define-key global-map [(f7)] (lambda () 
				(interactive)
				(setq debug-on-error t)))

(define-key global-map [(control c) (f)] 'find-function)


;;;; figlet
 
;; (defun figlet (start end)
;;   (interactive "r")
;;   (shell-command-on-region start end "figlet" nil t)
;;   (let ((cs (progn (backward-paragraph) (point)))
;; 	(ce (progn (forward-paragraph) (point))))
;;     (comment-region cs ce)))

;; ;;;; slime

;; (add-to-list 'load-path "~/elisp/slime") 



;; (setenv "SDL_VIDEO_FULLSCREEN_HEAD" "1")
(setenv "SBCL_HOME" "/usr/lib/sbcl")

(defun use-sbcl ()
  (interactive)
  (ignore-errors (slime-quit-lisp))
  (setq inferior-lisp-program "/usr/bin/sbcl"))

(defun use-ccl ()
  (interactive)
  (ignore-errors (slime-quit-lisp))
  (setq inferior-lisp-program "/home/dto/ccl/lx86cl"))

(defun use-ecl ()
  (interactive)
  (ignore-errors (slime-quit-lisp))
  (setq inferior-lisp-program "/usr/local/bin/ecl"))

(use-sbcl)

;;(add-to-list 'tramp-default-user-alist '("adb" nil "root"))
;; (add-to-list 'tramp-backup-directory-alist (cons "." "~/.emacs.d/backups/"))
;; (setq tramp-auto-save-directory "~/.emacs.d/auto-saves/")

(load (expand-file-name "~/quicklisp/slime-helper.el"))
;; (add-to-list 'load-path (expand-file-name "~/emacs-config/slime-2.14/"))
;; (load (expand-file-name "~/emacs-config/slime-2.14/slime.el"))
(slime-setup '(slime-fancy slime-asdf))
;;(slime-setup '(slime-autodoc slime-asdf slime-sprof slime-indentation slime-fancy))

;; (require 'info-look)
;; (add-to-list 'Info-directory-list (file-name-as-directory "/home/dto/gcl-info/"))
;; (setq Info-default-directory-list (cons "/usr/local/info/"  Info-default-directory-list))
;; (info-lookup-add-help
;;  :mode 'lisp-mode
;;  :regexp "[^][()'\" \t\n]+"
;;  :ignore-case t
;;  :doc-spec '(("(gcl.info)Symbol Index" nil nil nil)))

;; (add-to-list 'load-path (expand-file-name "~/gcl-info/"))
;; (require 'get-gcl-info)

;; This is my preference -- alter to taste.
(setq lisp-lambda-list-keyword-parameter-alignment t)
(setq lisp-lambda-list-keyword-alignment t)

(setf org-src-window-setup 'other-window)
                        
(setf frame-title-format "%b")

(setq org-export-html-style-include-default nil)
(setq org-agenda-files '("~/life.org"))
(setq org-log-done t)
(setq org-publish-timestamp-directory "~/.org-timestamps/")
(setq org-export-html-style-default "")
(setf org-export-html-postamble nil)
;(setf org-export-html-postamble-format nil)

(setf org-startup-folded nil)	 

;;;; misc

(display-time)
(setf display-time-day-and-date nil)
(setf display-time-24hr-format nil)
(setf display-time-use-mail-icon t)

;;;;; buffers and files

;; (global-set-key "\C-x\C-b" 'ibuffer)
;; (iswitchb-mode t)
;; (setq iswitchb-default-method 'samewindow)

;;;;; windows and frames

(winner-mode t)
(global-set-key [(control x) (left)] 'winner-undo)
(global-set-key [(control x) (right)] 'winner-redo)

;;;;; dired

(load "dired-x")  ;; the F command is cool
(setq dired-dwim-target t) ;; this is a cool option!

;;;;; eldoc

(autoload 'turn-on-eldoc-mode "eldoc" nil t)
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
(add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
(add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)

;;;; latex

(setq tex-dvi-view-command "xdvi")

;;;; keys

(global-set-key (kbd "<menu>") (lambda () (interactive) nil))

;;  (global-set-key (kbd "<Hangul>") (lambda () (interactive) nil))

;; (add-hook 'minibuffer-setup-hook
;;                (lambda ()
;; 		 (local-set-key (kbd "<Hangul>") (lambda () (interactive) nil))))


(global-set-key [(control tab)] 'other-window)

(add-hook 'org-mode-hook (lambda ()
			   (interactive)
			   (local-set-key [(control tab)] 'other-window)))
(global-set-key [(control shift tab)] (lambda () (interactive) (other-window -1)))

(add-hook 'magit-mode-hook (lambda ()
			   (interactive)
			   (local-set-key [(control tab)] 'other-window)))

;;;; visuals

(setq font-lock-maximum-decoration t)

;; fix for color-theme. ask zedek about this

;; (defun color-theme-face-attr-construct (face frame)
;;   (if (atom face)
;;       (custom-face-attributes-get face frame)
;;     (if (and (consp face) (eq (car face) 'quote))
;; 	(custom-face-attributes-get (cadr face) frame)
;;       (custom-face-attributes-get (car face)
;; 				  frame))))
;; ;; 

;; ;;;; scroll bars


;; (defun scroll-bars-in-this-frame (&optional off)
;;   (interactive)
;;   (modify-frame-parameters (selected-frame)
;; 			   `((vertical-scroll-bars . ,(if off
;; 							 nil
;; 							'left))
;; 			     (horizontal-scroll-bars . nil))))

;; (set-scroll-bar-mode t)

;; (scroll-bars-in-this-frame)

;;;; erc

;; (require 'erc)

;; (setq-default erc-auto-query t)
;; (setq-default erc-query-display 'bury)

;; (setf erc-modules '(autojoin 
;; 		    button 
;; 		    completion
;; 		    fill
;; 		    irccontrols
;; 		    log 
;; 		    match
;; 		    menu
;; 		    netsplit 
;; 		    noncommands
;; 		    notify 
;; 		    readonly 
;; 		    ring 
;; 		    scrolltobottom 
;; 		    services 
;; 		    smiley 
;; 		    stamp 
;; 		    track))

;; (setf erc-user-full-name "xxxxxx")
;; ;;(setf erc-user-full-name "|")
;; (setf erc-server "irc.freenode.net")
;; (setf erc-nick "dto")
;; (setf erc-nick-uniquifier "2")
;; (setf erc-away-nickname "away")
;; (setf erc-prompt "> ")
;; (setf erc-header-line-face-method t)
;; (setf erc-timestamp-format "%Y-%m-%d %H:%M")
;; (setf erc-track-visibility 'selected-visible)
;; (setf erc-track-position-in-mode-line 'right)

;; (setq-default indicate-empty-lines nil)
;; (setq-default indicate-buffer-boundaries nil)

;; (add-to-list 'auto-mode-alist '("\\.el" . emacs-lisp-mode))
;; (add-to-list 'auto-mode-alist '("\\.elo" . emacs-lisp-mode))
;; (add-to-list 'load-path "/home/dto/elo/")
;; (require 'elo)
;; (add-hook 'emacs-lisp-mode-hook 'elo-insinuate-emacs)


;; (setq erc-track-exclude-types '("JOIN" "NICK" "PART" "QUIT" "MODE"
;; 				"324" "329" "332" "333" "353" "477"))

;; (setq erc-track-exclude-server-buffer t)
;; (setq erc-track-shorten-aggressively nil)

;; (setf erc-autojoin-channels-alist 
;;       '(("freenode" "#xelf" "#lispgames" "##art" "##psychology")
;; 	("rizon" "#indiegamestand")))

;; (defun erc-all ()
;;   (interactive)
;;   (erc :server "irc.freenode.org")
;;   ;;(erc :server "im.codemonkey.be")
;;   (erc :server "irc.rizon.net" :nick "adeleradul"))
;;   ;; (erc :server "irc.quakenet.org"))
;;   ;; (erc :server "irc.slashnet.org"))
;;   ;; (erc :server "irc.globalgamers.net")
;;    ;; (erc :server "irc.esper.net"))

;; (require 'erc-log)

;; (setf erc-autoaway-message " autoaway after %i seconds idle. ")
;; (setf erc-notify-interval 30)

;; (setq erc-log-channels-directory "~/.emacs.d/logs")
;; (setq erc-log-insert-log-on-open nil)
;; (setq erc-save-buffer-on-part t
;;       erc-save-queries-on-quit nil
;;       erc-log-write-after-send t
;;       erc-log-write-after-insert t)

;; (erc-log-enable)

;; (setq org-export-with-section-numbers nil)

;;;; oddmuse

;; ;; Put this file in a directory on your `load-path' and 
;; ;; add this to your init file:
;; (require 'oddmuse)
;; (oddmuse-mode-initialize)
;; ;; And then use M-x oddmuse-edit to start editing.

(setf comment-style 'indent)


(recentf-mode 1)

;; (global-set-key "\C-cl" 'org-store-link)
;; (global-set-key "\C-ca" 'org-agenda)
;; (global-set-key "\C-cr" 'org-remember)
;; (global-set-key "\C-cb" 'bin)
;; (global-set-key "\C-cf" 'bin-find-page)

;; ;;(require 'org-mouse)

(setf org-src-fontify-natively t)

;; (setf erc-prompt-for-password nil)
;; (setf erc-prompt-for-nickserv-password nil)
;; (setf erc-server-auto-reconnect nil)
;; (setf org-archive-location "::* Archived Entries")			  
;; (add-hook 'org-mode-hook
;;                     (lambda () (imenu-add-to-menubar "Imenu")))
;; (setf org-imenu-depth 5)

;; (setf org-log-redeadline 'time)
;; (setf org-log-reschedule 'time)
;; (setf org-log-repeat 'time)

(provide 'init)

(menu-bar-mode -1)



;; 			ACHTUNG!

;; Das machine is nicht fur gerfingerpokenund mittengrabben.
;;      Ist easy schnappen der Sprinngwerk, blowenfusen und
;; 	       poppencorken mit spitzensparken.
;; Ist nicht fur gewerken by das Dummkopfen.  Das rubbernecken
;;     	 sightseeren keepen hands in das Pockets.
;; 	  Relaxen und watch das blinkenlights...

;; (setf org-todo-keyword-faces '(("NEXT" . (:foreground "yellow" :background "red" :bold t :weight bold))
;; 			       ("TODO" . (:foreground "yellow" :background "forestgreen" :bold t :weight bold))
;; 			       ("WAITING" . (:foreground "magenta" :background "red" :bold t :weight bold))
;; 			       ("TESTING" . (:foreground "cyan" :bold t :weight bold))
;; 			       ("WORKING" . (:foreground "yellow"  :bold t :weight bold))
;; 			       ("RELEASED" . (:foreground "greenyellow" :bold t :weight bold))
;; 			       ("PLANNED" . (:foreground "gray70" :bold t :weight bold))
;; 			       ("DONE" . (:foreground "goldenrod" :bold t :weight bold))))


;;;; org-publish

;(require 'org-publish-escript)

;; (setf backup-enable-predicate
;;       (lambda (fn) (not (file-remote-p fn))))


;; (defvar foo (impress-create-group 
;; 	     :name "test"
;; 	     :base-directory "~/notebook"
;; 	     :format :org
;; 	     :include "^.*\\.org$"))

;; (impress-get-group-files foo)
;; (impress-load-group-files foo)

;; (defvar bar (impress-create-path
;; 	     :name "org2html"
;; 	     :source-format :org
;; 	     :destination-format :html
;; 	     :destination-directory "~/test"
;; 	     :publisher #'impress-publish-org-to-html
;; 	     :options '(:headline-levels 3
;; 			:with-section-numbers nil
;; 			:table-of-contents nil
;; 			:style "<link rel=stylesheet href=\"/eon.css\" type=\"text/css\">"
;; 			:auto-preamble t
;; 			:auto-postamble t
;; 			:preamble "<div class=\"menu\" style=\"float: right;\">
;; <a href=\"/notebook\">home</a> - <a href=\"/notebook/pageindex.html\">index</a>
;; - <a href=\"/blog\">blog</a></div>"
;; 			:base-directory "~/notebook/"
;; 			:base-extension "org"
;; 			:auto-index t
;; 			:index-filename "pageindex.org")))

;; (impress-publish foo bar)

;; (require 'htmlfontify)

;; (defun publish-clon-to-html ()
;;   (interactive)
;;   (let ((hfy-find-cmd "find . -type f -name \"*.lisp\""))
;;     (htmlfontify-copy-and-link-dir "~/clon/" "~/html/clon/"
;; 				   ".lisp" ".html")))

;; (defun spaces-to-dashes (string)
;;   (save-match-data
;;     (with-temp-buffer
;;       (insert string)
;;       (goto-char (point-min))
;;       (while (re-search-forward " " nil t)
;; 	(replace-match "-"))
;;       (buffer-substring-no-properties (point-min) (point-max)))))

;; (defun spaces-to-underscores (string)
;;   (save-match-data
;;     (with-temp-buffer
;;       (insert string)
;;       (goto-char (point-min))
;;       (while (re-search-forward " \\|-" nil t) ;; and dashes!
;; 	(replace-match "_"))
;;       (buffer-substring-no-properties (point-min) (point-max)))))

;; (defun publish-lisp-to-html (plist filename pub-dir)
;;   (interactive)
;;     (let* ((dir (file-name-directory filename))
;; 	   (base (file-name-sans-extension (file-name-nondirectory filename)))
;; 	   (target (expand-file-name (concat base ".html") pub-dir))
;; 	   (html nil)
;; 	   (hfy-src-doc-link-style "color: #a020f0; font-weight: bold;")
;; 	   (hfy-sec-doc-link-unstyle " color: #4d4d4d;"))
;;       (with-current-buffer (find-file-noselect filename)
;; 	(hfy-force-fontification)
;; 	(setf html (hfy-fontify-buffer dir filename)))
;;       (with-current-buffer html
;; 	;; add anchors for all three-semicolon headings
;; 	(goto-char (point-min))
;; 	(while (re-search-forward ";;;[^@]*@@ \\([^<>]*\\)" nil t)
;; 	  (message "matched %s" (match-string-no-properties 1))
;; 	  (replace-match (format ";;; <a name=\"%s\">@@ %s</a>"
;; 				 (spaces-to-underscores (match-string-no-properties 1))
;; 				 (match-string-no-properties 1))))
;; 	(write-file target nil))
;;       (kill-buffer html)))

;;(setq org-html-head "")
(setq org-html-head "<link rel='stylesheet' href=\"./eon.css\" type=\"text/css\"/>")

(setq org-publish-project-alist
       `(
	 ("webroot"
	  :with-section-numbers nil
	  :table-of-contents nil
	  :style "<link rel=stylesheet href=\"/e/freeshell2.css\" type=\"text/css\"/>"
	  :base-directory "~"
	  :base-extension "css\\|js"
	  :publishing-function org-publish-attachment
	  :auto-postamble nil
	  :auto-preamble t
	  :publishing-directory "~/html/")
	 ("notebook" 
	 :headline-levels 3
	 :with-section-numbers nil
	 :table-of-contents nil
	 :style "<link rel='stylesheet' href=\"/eon.css\" type=\"text/css\"/><link href=\"https://fonts.googleapis.com/css?family=Fira+Sans:400,700&display=swap\" rel=\"stylesheet\">"
	 :auto-preamble t
	 :auto-postamble nil
	 :preamble "<div class=\"menu\" style=\"float: right;\">
<a href=\"/notebook\">home</a> - <a href=\"/notebook/pageindex.html\">index</a>
 - </div>"
	 :base-directory "~/dto.github.com/notebook/"
	 :base-extension "org"
	 :publishing-directory "~/dto.github.com/notebook/"
	 ;; :publishing-function (org-publish-org-to-html)
	 :auto-index t
	 :index-filename "pageindex.org")
	 ("xe2-org" 
	 :headline-levels 3
	 :with-section-numbers t
	 :table-of-contents t
	 :style "<link rel=stylesheet href=\"/eon.css\" type=\"text/css\">"
	 :auto-preamble t
	 :auto-postamble nil
	 :base-directory "~/xe2/"
	 :base-extension "org"
	 :publishing-directory "~/html/notebook/"
	 :publishing-function (org-publish-org-to-html org-publish-attachment))
	 ("docs" 
	 :headline-levels 3
	 :with-section-numbers nil
	 :table-of-contents nil
	 :base-directory "~/xelf/doc/"
	 :base-extension "org"
	 :publishing-directory "~/xelf/doc/"
	 :publishing-function org-html-publish-to-html
	 :auto-index nil
	 :index-filename "pageindex.org")
	 ("source" 
	 :headline-levels 3
	 :with-section-numbers nil
	 :table-of-contents nil
	 :base-directory "~/xelf/"
	 :base-extension "org"
	 :publishing-directory "~/xelf/doc/"
	 :publishing-function org-html-publish-to-html
	 :auto-index nil
	 :index-filename "pageindex.org")
	 ("test" 
	 :headline-levels 3
	 :with-section-numbers t
	 :table-of-contents t
	 :base-directory "~/elisp/org-html-themes/"
	 :base-extension "org"
	 :publishing-directory "~/elisp/org-html-themes/"
	 :publishing-function org-html-publish-to-html
	 :auto-index nil
	 :index-filename "pageindex.org")
	 ("dictionary" 
	 :headline-levels 3
	 :with-section-numbers nil
	 :table-of-contents nil
	 :base-directory "~/xelf/doc/dictionary/"
	 :base-extension "org"
	 :publishing-directory "~/xelf/doc/dictionary/"
	 :publishing-function org-html-publish-to-html
	 :auto-index nil
	 :index-filename "pageindex.org")
	 ("ioweb" 
	 :headline-levels 3
	 :with-section-numbers nil
	 :table-of-contents nil
	 :style "<link rel='stylesheet' href=\"/eon.css\" type=\"text/css\"/><link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>"
	 :preamble "<link rel='stylesheet' href=\"/eon.css\" type=\"text/css\"/><link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>"
	 :base-directory "~/ioweb"
	 :base-extension "org"
	 :publishing-directory "~/ioweb"
	 :publishing-function org-html-publish-to-html
	 :auto-index nil
	 :index-filename "pageindex.org")
	 ("intr" 
	 :headline-levels 3
	 :with-section-numbers nil
	 :table-of-contents nil
	 :style "<link rel='stylesheet' href=\"/eon.css\" type=\"text/css\"/><link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>"
	 :preamble "<link rel='stylesheet' href=\"/eon.css\" type=\"text/css\"/><link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>"
	 :base-directory "~/intr"
	 :base-extension "org"
	 :publishing-directory "~/intr"
	 :publishing-function org-html-publish-to-html
	 :auto-index nil
	 :index-filename "pageindex.org")
	 ))

(defun book-margins-on ()
  (interactive)
  (set-window-margins nil 16 16))

(defun book-margins-off ()
  (interactive)
  (set-window-margins nil 0 0))

(defun book-big-margins-on ()
  (interactive)
  (set-window-margins nil 32 32))

      ;; ("ioweb-css"
	 ;;  :with-section-numbers nil
	 ;;  :table-of-contents nil
	 ;;  :base-directory "~"
	 ;;  :base-extension "css\\|js"
	 ;;  :publishing-function org-publish-attachment
	 ;;  :auto-postamble nil
	 ;;  :auto-preamble t
	 ;;  :publishing-directory "~/ioweb/")))
	 ;; ("ioweb-images" 
	 ;; :base-directory "~/ioweb/"
	 ;; :base-extension "png\\|jpg\\|gif"
	 ;; :publishing-function org-publish-attachment
	 ;; :publishing-directory "~/ioweb")))

(setq org-export-with-toc nil)

;; (add-to-list 'load-path "/home/dto/src/screenwriter-1.6.6")
;; (require 'screenwriter)
;; (setq auto-mode-alist (cons '("\\.scp" . screenwriter-mode) auto-mode-alist))
;; (setq auto-mode-alist (cons '("\\.screenplay" . screenwriter-mode) auto-mode-alist))

(defun kill-sbcl ()
  (interactive)
  (slime-quit-lisp t))

(defun kill-snd ()
  (interactive)
  (snd-kill))


;; (defun chicken-doc (&optional obtain-function)
;;   (interactive)
;;   (let ((func (funcall (or obtain-function 'current-word))))
;;     (when func
;;       (process-send-string (scheme-proc)
;;                            (format "(require-library chicken-doc) ,doc %S\n" func))
;;       (save-selected-window
;;         (select-window (display-buffer (get-buffer scheme-buffer) t))
;;         (goto-char (point-max))))))
  
;; (eval-after-load 'cmuscheme
;;  '(define-key scheme-mode-map "\C-cd" 'chicken-doc))

;; ;; Additionally, because multiple matches may be listed, this snippet will allow you to place your cursor at the
;; ;; beginning of the match s-expression and get the actual documentation:

;; (eval-after-load 'cmuscheme
;;  '(define-key inferior-scheme-mode-map "\C-cd"
;;     (lambda () (interactive) (chicken-doc 'sexp-at-point))))

;; (require 'cmuscheme)
;; (setq scheme-program-name "csi -:c")

;; (defun scheme-module-indent (state indent-point normal-indent) 0)
;; (put 'module 'scheme-indent-function 'scheme-module-indent)

;; (put 'and-let* 'scheme-indent-function 1)
;; (put 'parameterize 'scheme-indent-function 1)
;; (put 'handle-exceptions 'scheme-indent-function 1)
;; (put 'when 'scheme-indent-function 1)
;; (put 'unless 'scheme-indent-function 1)
;; (put 'match 'scheme-indent-function 1)

;; (define-key scheme-mode-map "\C-c\M-l" 'scheme-load-current-file)
;; (define-key scheme-mode-map "\C-c\M-k" 'scheme-compile-current-file)

;; (defun scheme-load-current-file (&optional switch)
;;   (interactive "P")
;;   (let ((file-name (buffer-file-name)))
;;     (comint-check-source file-name)
;;     (setq scheme-prev-l/c-dir/file (cons (file-name-directory    file-name)
;; 					 (file-name-nondirectory file-name)))
;;     (comint-send-string (scheme-proc) (concat "(load \""
;; 					      file-name
;; 					      "\"\)\n"))
;;     (if switch
;;       (switch-to-scheme t)
;;       (message "\"%s\" loaded." file-name) ) ) )

;; (defun scheme-compile-current-file (&optional switch)
;;   (interactive "P")
;;   (let ((file-name (buffer-file-name)))
;;     (comint-check-source file-name)
;;     (setq scheme-prev-l/c-dir/file (cons (file-name-directory    file-name)
;; 					 (file-name-nondirectory file-name)))
;;     (message "compiling \"%s\" ..." file-name)
;;     (comint-send-string (scheme-proc) (concat "(compile-file \""
;; 					      file-name
;; 					      "\"\)\n"))
;;     (if switch
;;       (switch-to-scheme t)
;;       (message "\"%s\" compiled and loaded." file-name) ) ) )
;; ;; (require 'geiser)
;; ;; (require 'geiser-chicken)
;; ;; (require 'chicken-scheme)
;; ;; (add-hook 'scheme-mode-hook 'setup-chicken-scheme)
;; ;; (define-key scheme-mode-map (kbd "C-?") 'chicken-show-help)
;; ;; (setq geiser-active-implementations '(chicken))
;; ;; ;;(load "/home/dto/.emacs.d/elpa/geiser-20151226.1914/geiser.el")
;; ;; (require 'geiser)

;; ;; (set-frame-font "Mono 12")
;; ;; (defun large-font ()
;; ;;   (interactive)
;; ;;   (set-frame-font "Mono 13"))
;; ;; (defun small-font ()
;; ;;   (interactive)
;; ;;   (set-frame-font "Mono 10"))

;; (large-font)

;;(global-highlight-changes-mode t)

(global-set-key '[M-right] 'highlight-changes-next-change)
(global-set-key '[M-left]  'highlight-changes-previous-change)

(defun toggle-fullscreen (&optional f)
  (interactive)
  (let ((current-value (frame-parameter nil 'fullscreen)))
    (set-frame-parameter nil 'fullscreen
			 (if (equal 'fullboth current-value)
			     (if (boundp 'old-fullscreen) old-fullscreen nil)
			     (progn (setq old-fullscreen current-value)
				    'fullboth)))))

;; (defvar *project* "2fr0g")

;; (defun recent-lisp-buffers ()
;;   (remove-if-not #'(lambda (s) (string-match (rx (one-or-more word) ".lisp") s))
;; 		 (mapcar #'buffer-name (buffer-list))))

;; (defun last-lisp-buffer ()
;;   (first (recent-lisp-buffers)))

;; (defun* switch-to-slime-layout (&optional (primary-buffer (last-lisp-buffer)))
;;   (interactive) 
;;   (delete-other-windows)
;;   (switch-to-buffer primary-buffer)
;;   (split-window-right) 
;;   (other-window 1)
;;   (switch-to-buffer "todo.org") 
;;   (split-window-below)
;;   (other-window 1)
;;   (when (not (get-buffer "*slime-repl sbcl*"))
;;     (save-window-excursion (slime)))
;;   (switch-to-buffer "*slime-repl sbcl*")
;;   (balance-windows))

;;; Keys related to changes 

(global-set-key (kbd "<f1>") 
		(lambda () (interactive)
		  (if (symbol-at-point)
		      (Info-goto-node (concat "(gcl.info)" (symbol-name (symbol-at-point))))
		      (get-gcl-info))))

(global-unset-key (kbd "<f2>"))

(global-set-key (kbd "<f2>") #'(lambda () (interactive) (highlight-changes-mode -1)))
(global-set-key (kbd "<f3>") #'(lambda () (interactive) (highlight-changes-mode 1)))

(global-set-key (kbd "<f4>") 'highlight-changes-visible-mode)

(global-set-key (kbd "<f5>") 'ibuffer)

;; (global-set-key (kbd "<f6>") 'winner-undo)
;; (global-set-key (kbd "<f7>") 'switch-to-slime-layout)
;; (global-set-key (kbd "<f8>") 'winner-redo)

;; (global-set-key (kbd "<f9>") #'(lambda ()
;; 				 (interactive)
;; 				 (slime-connect "192.168.1.171" 4005) 
;; 				 ))
;; (global-set-key (kbd "M-<f9>") '(lambda () (interactive) 
;; 				 (slime-compile-file t)))

;; (global-set-key (kbd "<f10>") (lambda () (interactive) 
;; 			       (slime-load-system *project*)))

(global-set-key [f11] 'toggle-fullscreen)


(global-set-key (kbd "<f6>") (lambda () (interactive) (book-margins-off)))
(global-set-key (kbd "<f7>") (lambda () (interactive) (book-margins-on)))
(global-set-key (kbd "S-<f7>") (lambda () (interactive) (book-mini-margins-on)))
(global-set-key (kbd "M-<f12>") (lambda () (interactive) (window-configuration-to-register "j")))
(global-set-key (kbd "<f12>") (lambda () (interactive) (jump-to-register "j")))

(global-set-key (kbd "s-k") (lambda () (interactive) (kill-sbcl)))


;;     (setq buffer-menu-buffer-font-lock-keywords
;;       '(("^....[*]Man .*Man.*"   . font-lock-variable-name-face) ;Man page
;;         (".*Dired.*"             . font-lock-comment-face)       ; Dired
;;         ("^....[*]shell.*"       . font-lock-preprocessor-face)  ; shell buff
;;         (".*[*]scratch[*].*"     . font-lock-function-name-face) ; scratch buffer
;;         ("^....[*].*"            . font-lock-string-face)        ; "*" named buffers
;;         ("^..[*].*"              . font-lock-constant-face)      ; Modified
;;         ("^.[%].*"               . font-lock-keyword-face)))     ; Read only

;;     (defun buffer-menu-custom-font-lock  ()
;;       (let ((font-lock-unfontify-region-function
;;              (lambda (start end)
;;                (remove-text-properties start end '(font-lock-face nil)))))
;;         (font-lock-unfontify-buffer)
;;         (set (make-local-variable 'font-lock-defaults)
;;              '(buffer-menu-buffer-font-lock-keywords t))
;;         (font-lock-fontify-buffer)))

;;     (add-hook 'buffer-menu-mode-hook 'buffer-menu-custom-font-lock)

;; ;; http://users-phys.au.dk/harder/dpans.html

;; (add-to-list 'auto-mode-alist
;; 	     '("/\\(rfc\\|std\\)[0-9]+\\.txt\\'" . rfcview-mode))

;; (autoload 'rfcview-mode "rfcview" nil t)

;; (load-file "/home/dto/secrets.el")

;; ;(require 'indicators)

;; (setf bm-restore-repository-on-load t)
;; (require 'bm)

;; (add-hook 'find-file-hooks 'bm-buffer-restore)
;; (add-hook 'kill-buffer-hook 'bm-buffer-save)

;; (add-hook 'kill-emacs-hook '(lambda nil
;;                                    (bm-buffer-save-all)
;;                                    (bm-repository-save)))

;; (add-hook 'after-save-hook 'bm-buffer-save)
;; (add-hook 'after-revert-hook 'bm-buffer-restore)

;; (add-hook 'vc-before-checkin-hook 'bm-buffer-save)

;; (global-set-key (kbd "M-\\") 'bm-toggle) 
;; (global-set-key (kbd "M-]") 'bm-next)
;; (global-set-key (kbd "M-[") 'bm-previous)
;; (global-set-key (kbd "M-'") 'bm-show-all)

;; (global-set-key (kbd "<left-fringe> <mouse-5>") 'bm-next-mouse)
;; (global-set-key (kbd "<left-fringe> <mouse-4>") 'bm-previous-mouse)
;; (global-set-key (kbd "<left-fringe> <mouse-1>") 'bm-toggle-mouse)

;; (setq bm-in-lifo-order t)
;; (setq bm-cycle-all-buffers t)
;; (setq bm-highlight-style 'bm-highlight-only-fringe)



(require 'sr-speedbar)
(global-set-key (kbd "<f1>") 'sr-speedbar-toggle)
;; ;;(sr-speedbar-refresh-turn-off)
(global-set-key (kbd "s-=") 'speedbar-refresh)
 (setf sr-speedbar-right-side t)

;; (speedbar-add-supported-extension ".lisp")

;; (setf imenu-lisp-other-defn-regexp
;;       (concat "^\\s-*("
;;               (regexp-opt '("defgroup" "deftheme" "deftype" "defstruct"
;;                             "defclass" "define-condition" "define^"
;; 			    "define-method^" "define-class^" "defblock"
;;                             "defpackage")
;; 			  t)
;;               "\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)"))

;; (require 'org-projectile)
;; (org-projectile:per-repo)
;; (setq org-projectile:per-repo-filename "project.org")
;; (setq org-agenda-files (append org-agenda-files (org-projectile:todo-files)))
;; (global-set-key (kbd "C-c c") 'org-capture)
;; (global-set-key (kbd "C-c n p") 'org-projectile:project-todo-completing-read)

;; (projectile-global-mode)

;; (require 'helm)
;; (require 'helm-config)

;; ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
;; (global-set-key (kbd "C-c h") 'helm-command-prefix)
;; (global-unset-key (kbd "C-x c"))

;; (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
;; (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
;; (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

;; ;;(when (executable-find "curl")
;; ;;  (setq helm-google-suggest-use-curl-p t))

;; (setq helm-split-window-in-side-p           nil ; open helm buffer inside current window, not occupy whole other window
;;       helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
;;       helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
;;       helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
;;       helm-ff-file-name-history-use-recentf t)

;; (helm-mode 1)

;; (setq helm-exit-idle-delay 0)
;; (require 'helm-projectile)
;; (helm-projectile-on)

;; (require 'org-bullets)
;; (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

;; (setf org-bullets-bullet-list '("●" "◆" "◇" "○"))
;; ;; (require 'ob-sh)
;; ;; (require 'ob-lisp)

;; ♥ ● ◇ ✚ ✜ ☯ ◆ ♠ ♣ ♦ ☢ ❀ ◆ ◖ ▶
    ;;; Small
;; ► • ★ ▸
;; "◉"
;;     "○"
;;     "✸"
;;     "✿"

(desktop-save-mode 1)

(add-to-list 'desktop-globals-to-save 'file-name-history)
(add-hook 'auto-save-hook (lambda () (desktop-save-in-desktop-dir)))
;; (setq org-src-fontify-natively t)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((lisp . t)))

;; (require 'ob-sh)
;; (require 'ob-lisp)

(add-to-list 'custom-theme-load-path "~/xelf")
(add-to-list 'custom-theme-load-path "~/emacs-config")
;;(load "~/emacs-config/xiomacs-theme.el")

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Orgmode stuff

;; (add-hook 'org-mode-hook (lambda () (variable-pitch-mode)))

;; (defun my-adjoin-to-list-or-symbol (element list-or-symbol)
;;   (let ((list (if (not (listp list-or-symbol))
;;                   (list list-or-symbol)
;;                 list-or-symbol)))
;;     (require 'cl-lib)
;;     (cl-adjoin element list)))

;; (eval-after-load "org"
;;   '(mapc
;;     (lambda (face)
;;       (set-face-attribute
;;        face nil
;;        :inherit
;;        (my-adjoin-to-list-or-symbol
;;         'fixed-pitch
;;         (face-attribute face :inherit))))
;;     (list 'org-code 'org-block 'org-table)))

(global-set-key (kbd "<C-return>") (lambda () (interactive) (other-window 1)))
(setf org-src-preserve-indentation t)

;; func copied here from org sources with patch for SLIME

;; (defun org-src--contents-for-write-back ()
;;   "Return buffer contents in a format appropriate for write back.
;; Assume point is in the corresponding edit buffer."
;;   (let ((indentation (or org-src--block-indentation 0))
;; 	(preserve-indentation org-src--preserve-indentation)
;; 	(contents (org-with-wide-buffer (buffer-string)))
;; 	(write-back org-src--allow-write-back))
;;     (with-temp-buffer
;;       (insert (org-no-properties contents))
;;       (goto-char (point-min))
;;       ;; this line commented out to work around bug
;;       ;; (when (functionp write-back) (funcall write-back))
;;       (unless (or preserve-indentation (= indentation 0))
;; 	(let ((ind (make-string indentation ?\s)))
;; 	  (goto-char (point-min))
;; 	  (while (not (eobp))
;; 	    (when (looking-at-p "[ \t]*\\S-") (insert ind))
;; 	    (forward-line))))
;;       (buffer-string))))



(setq visible-bell t) 
(setq ring-bell-function
      (lambda ()
        (let ((orig-fg (face-foreground 'mode-line)))
          (set-face-foreground 'mode-line "#aaaaaa")
          (run-with-idle-timer 0.1 nil
                               (lambda (fg) (set-face-foreground 'mode-line fg))
                               orig-fg))))
(global-set-key (kbd "C-`") 'company-complete)

(require 'slime-company)
(slime-setup '(slime-company))

(defun add-pcomplete-to-slime ()
  (add-hook 'completion-at-point-functions 'pcomplete-completions-at-point nil t))

(add-hook 'org-mode-hook #'add-pcomplete-to-slime)

;; (defun duality ()
;;   (interactive)
;;   (load "~/.emacs.d/duality-theme.el"))

;; (run-at-time 2 nil #'duality)

(add-to-list 'auto-mode-alist '("\\.diary" . diary-mode))

(add-hook 'diary-list-entries-hook 'diary-include-other-diary-files)
     (add-hook 'diary-mark-entries-hook 'diary-mark-included-diary-files)

     (setq calendar-latitude 42.19)
     (setq calendar-longitude -71.38)
(setq calendar-location-name "Northborough, Massachusetts")

(setq erc-auto-query 'bury)

(add-to-list 'auto-mode-alist (cons "\\.paren\\'" 'lisp-mode))
(add-hook 'lisp-mode-hook
          #'(lambda ()
              (when (and buffer-file-name
                         (string-match-p "\\.paren\\>" buffer-file-name))
                (unless (slime-connected-p)
                  (save-excursion (slime)))
                (trident-mode +1))))
;; (trident-add-keys-with-prefix "C-c C-e")

;;;;;;;;;;;;;;;;;;;;;; SND SCHEME


(global-set-key (kbd "<Scroll_Lock>")
		(lambda ()
		  (interactive)
		  (call-interactively 'menu-bar-mode)))

(global-set-key (kbd "<menu>") 'ibuffer)
		

(add-hook 'snd-scheme-mode-hook #'xelf-do-font-lock)
;(global-set-key [(f1)] 'mosaic-go)

;; (set-frame-font "Fira Code Light")
;; (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol")

;; (defun my-correct-symbol-bounds (pretty-alist)
;;     "Prepend a TAB character to each symbol in this alist,
;; this way compose-region called by prettify-symbols-mode
;; will use the correct width of the symbols
;; instead of the width measured by char-width."
;;     (mapcar (lambda (el)
;;               (setcdr el (string ?\t (cdr el)))
;; 	      el)
;;             pretty-alist))

;;   (defun my-ligature-list (ligatures codepoint-start)
;;     "Create an alist of strings to replace with
;; codepoints starting from codepoint-start."
;;     (let ((codepoints (-iterate '1+ codepoint-start (length ligatures))))
;;       (-zip-pair ligatures codepoints)))

;; (defun my-set-fira-code-ligatures ()
;;   (interactive)
;;   (setq-local prettify-symbols-alist
;; 	(append my-fira-code-ligatures prettify-symbols-alist))
;;   (prettify-symbols-mode))

(defun make-prettify-alist (list)
  "Generate prettify-symbols alist from LIST."
  (let ((idx -1))
    (mapcar
     (lambda (s)
       (setq idx (1+ idx))
       (let* ((code (+ #Xe100 idx))
	      (width (string-width s))
	      (prefix ())
	      (suffix '(?\s (Br . Br)))
	      (n 1))
	 (while (< n width)
	   (setq prefix (append prefix '(?\s (Br . Bl))))
	   (setq n (1+ n)))
	 (cons s (append prefix suffix (list (decode-char 'ucs code))))))
     list)))

;; (setq my-fira-code-ligatures
;;       (let* ((ligs '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\"
;;                   "{-" "[]" "::" ":::" ":=" "!!" "!=" "!==" "-}"
;;                   "--" "---" "-->" "->" "->>" "-<" "-<<" "-~"
;;                   "#{" "#[" "##" "###" "####" "#(" "#?" "#_" "#_("
;;                   ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*"
;;                   "/**" "/=" "/==" "/>" "//" "///" "&&" "||" "||="
;;                   "|=" "|>" "^=" "$>" "++" "+++" "+>" "=:=" "=="
;;                   "===" "==>" "=>" "=>>" "<=" "=<<" "=/=" ">-" ">="
;;                   ">=>" ">>" ">>-" ">>=" ">>>" "<*" "<*>" "<|" "<|>"
;;                   "<$" "<$>" "<!--" "<-" "<--" "<->" "<+" "<+>" "<="
;;                   "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<" "<~"
;;                   "<~~" "</" "</>" "~@" "~-" "~=" "~>" "~~" "~~>" "%%"
;;                   "x" ":" "+" "+" "*")))
;; 	(my-correct-symbol-bounds (my-ligature-list ligs #Xe100))))

;; (defconst fira-code-mode--ligatures
;;   '("www" "**" "***" "**/" "*>" "*/" "\\\\" "\\\\\\"
;;     "{-" "[]" "::" ":::" ":=" "!!" "!=" "!==" "-}"
;;     "--" "---" "-->" "->" "->>" "-<" "-<<" "-~"
;;     "#{" "#[" "##" "###" "####" "#(" "#?" "#_" "#_("
;;     ".-" ".=" ".." "..<" "..." "?=" "??" ";;" "/*"
;;     "/**" "/=" "/==" "/>" "//" "///" "&&" "||" "||="
;;     "|=" "|>" "^=" "$>" "++" "+++" "+>" "=:=" "=="
;;     "===" "==>" "=>" "=>>" "<=" "=<<" "=/=" ">-" ">="
;;     ">=>" ">>" ">>-" ">>=" ">>>" "<*" "<*>" "<|" "<|>"
;;     "<$" "<$>" "<!--" "<-" "<--" "<->" "<+" "<+>" "<="
;;     "<==" "<=>" "<=<" "<>" "<<" "<<-" "<<=" "<<<" "<~"
;;     "<~~" "</" "</>" "~@" "~-" "~=" "~>" "~~" "~~>" "%%"
;;     "x" ":" "+" "+" "*"))

;; (defvar fira-code-mode--old-prettify-alist)

;; (defun fira-code-mode--enable ()
;;   "Enable Fira Code ligatures in current buffer."
;;   (setq-local fira-code-mode--old-prettify-alist prettify-symbols-alist)
;;   (setq-local prettify-symbols-alist (append my-fira-code-ligatures fira-code-mode--old-prettify-alist (list '("lambda" . ?λ))))
;;   (prettify-symbols-mode t))

;; (defun fira-code-mode--disable ()
;;   "Disable Fira Code ligatures in current buffer."
;;   (setq-local prettify-symbols-alist fira-code-mode--old-prettify-alist)
;;   (prettify-symbols-mode -1))

;; (define-minor-mode fira-code-mode
;;   "Fira Code ligatures minor mode"
;;   :lighter " Fira Code"
;;   (setq-local prettify-symbols-unprettify-at-point 'right-edge)
;;   (setq-local prettify-symbols-compose-predicate
;; 	      (lambda (start end _match)
;; 		;; don't replace single characters
;; 		(>= (- end start) 2)))
;;   (if fira-code-mode
;;       (fira-code-mode--enable)
;;     (fira-code-mode--disable)))

;; (defun fira-code-mode--setup ()
;;   "Setup Fira Code Symbols"
;;   (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol"))

;; (provide 'fira-code-mode)

;;;;;;;;;;;;

;; (defun unicode-symbol (name)
;;     "Translate a symbolic name for a Unicode character -- e.g., LEFT-ARROW
;;   or GREATER-THAN into an actual Unicode character code. "
;;     (decode-char 'ucs (case name
;;                         ;; arrows 
;; 			('cloverleaf #X2318)
;; 			('up-right-arrow #X2197)
;;                         ('left-arrow 8592)
;;                         ('up-arrow 8593)
;;                         ('right-arrow 8594)
;;                         ('down-arrow 8595)
;; 			('white-diamond #X25c7)
;;                         ;; boxes
;;                         ('double-vertical-bar #X2551)
;;                         ;; relational operators
;;                         ('equal #X003d)
;;                         ('not-equal #X2260)
;;                         ('identical #X2261)
;;                         ('not-identical #X2262)
;;                         ('less-than #X003c)
;;                         ('greater-than #X003e)
;;                         ('less-than-or-equal-to #X2264)
;;                         ('greater-than-or-equal-to #X2265)
;;                         ;; logical operators
;;                         ('logical-and #X2227)
;;                         ('logical-or #X2228)
;;                         ('logical-neg #X00AC)
;;                         ;; misc
;;                         ('nil #X2205)
;;                         ('horizontal-ellipsis #X2026)
;;                         ('double-exclamation #X203C)
;;                         ('prime #X2032)
;;                         ('double-prime #X2033)
;;                         ('for-all #X2200)
;;                         ('there-exists #X2203)
;;                         ('element-of #X2208)
;;                         ;; mathematical operators
;;                         ('square-root #X221A)
;;                         ('squared #X00B2)
;;                         ('cubed #X00B3)
;;                         ;; letters
;;                         ('lambda #X03BB)
;;                         ('alpha #X03B1)
;;                         ('beta #X03B2)
;;                         ('gamma #X03B3)
;;                         ('delta #X03B4))))

;;  (defun substitute-pattern-with-unicode (pattern symbol &optional raw)
;;     "Add a font lock hook to replace the matched part of PATTERN with the 
;;   Unicode symbol SYMBOL looked up with UNICODE-SYMBOL."
;;     (interactive)
;;     (font-lock-add-keywords
;;      nil `((,pattern (0 (progn (compose-region (match-beginning 1) (match-end 1)
;;                                               ,(unicode-symbol symbol))
;;                               nil))))))
  
;;   (defun substitute-patterns-with-unicode (patterns)
;;     "Call SUBSTITUTE-PATTERN-WITH-UNICODE repeatedly."
;;     (mapcar #'(lambda (x)
;;                 (substitute-pattern-with-unicode (car x)
;;                                                  (cdr x)))
;;             patterns))

;; (defun dto/prettify-lisp ()
;;   (interactive)
;;   (substitute-patterns-with-unicode
;;    '(("define\\(\\^\\)" . cloverleaf)
;;      ("define-class\\(\\^\\)" . cloverleaf)
;;      ("define-method\\(\\^\\)" . cloverleaf)
;;      ("(\\(<=\\)" . less-than-or-equal-to)
;;      ("(\\(>=\\)" . greater-than-or-equal-to)
;;      ("\\(lambda\\)" . lambda)
;;      ("\\(lambda\\)\\*" . lambda)
;;      ("(\\(and\\) " . logical-and)
;;      ("(\\(or\\) " . logical-or)
;;      ("(\\(not\\) " . logical-not)
;;      ("\\(<>\\)" . white-diamond))))

;; (add-hook 'emacs-lisp-mode-hook 'dto/prettify-lisp)
;; (add-hook 'snd-scheme-mode-hook 'dto/prettify-lisp)

;; (add-to-list 'load-path "/home/dto/Desktop/mosaic/")
;; (require 'mosaic)


(global-set-key (kbd "<pause>") (lambda () (interactive) (setq debug-on-error t)))

(defun snd-scheme-insinuate-imenu ()
  (interactive)
  (setf
   imenu-generic-expression
   `(("Definitions" "^\\s-*(define\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Functions" "^\\s-*(define\\^\\s-+'?(\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Macros" "^\\s-*(define-macro\\s-+'?(\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Methods" "^\\s-*(define-method\\^\\s-+'?(\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Classes" "^\\s-*(define-class\\^\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1))))

(defun emacs-lisp-insinuate-imenu ()
  (interactive)
  (setf
   imenu-generic-expression
   `(("Variables" "^\\s-*(defvar\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Functions" "^\\s-*(defun\\*?\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Macros" "^\\s-*(defmacro\\*?\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Methods" "^\\s-*(defmethod\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 1)
     ("Classes" "^\\s-*(\\(cell-\\)?defclass\\s-+'?\\(\\sw\\(\\sw\\|\\s_\\)+\\)" 2))))

(add-hook 'emacs-lisp-mode-hook #'imenu-add-menubar-index)
(add-hook 'emacs-lisp-mode-hook #'emacs-lisp-insinuate-imenu)
(add-hook 'lisp-mode-hook #'imenu-add-menubar-index)
(add-hook 'snd-scheme-mode-hook #'snd-scheme-insinuate-imenu)
(add-hook 'snd-scheme-mode-hook #'imenu-add-menubar-index)

(run-at-time 2.0 nil (lambda ()
		       (set-frame-font "Fira Code Light 13")))

