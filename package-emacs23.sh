#!/bin/bash
# package-emacs23.sh  
# badly written packager for emacs23

VERSION="23.0.50.2"

# this is a hack. need a better solution.

apt-get --yes build-dep emacs22

# Download CVS emacs from savannah

cd ~/src
export CVS_RSH=ssh
cvs -z3 -d:pserver:anonymous@cvs.sv.gnu.org:/cvsroot/emacs co emacs

rm -rf emacs23-$VERSION
mv emacs emacs23-$VERSION
cd emacs23-$VERSION

#./configure --with-xft --enable-font-backend --without-toolkit-scroll-bars --with-x-toolkit=lucid --prefix=/opt
./configure --with-xft --enable-font-backend --with-x-toolkit=gtk --with-rsvg --prefix=/opt --without-toolkit-scroll-bars

make bootstrap && \
checkinstall --pkgname=emacs23 --pkgversion=$VERSION  \
    -A "any" \
    --deldoc --deldesc --delspec \
    --type=debian --pkglicense=gpl --pkggroup=x2os \
    --default \
    --maintainer="dto@gnu.org" 
